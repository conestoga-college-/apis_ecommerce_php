<?php
    if($isConnectedToDB) {
        $sql = $db->prepare('SELECT id, name, image, description, price, shipping_cost FROM products');
        $sql->execute();
        
        $resultProducts = array();

        if($products = $sql->fetchAll(PDO::FETCH_ASSOC)) {       
            foreach($products as $product) {
                $resultProduct = new stdClass();
                $resultProduct->id = $product['id'];
                $resultProduct->name = $product['name'];
                $resultProduct->price = $product['price'];
                $resultProduct->image = $product['image'];
                array_push($resultProducts, $resultProduct);
            }

            echo json_encode($resultProducts);
        }
        else{   
            $response = new stdClass();
            $response->message = "There is no product to be shown.";
            echo json_encode($response);
        }
    }
?>
