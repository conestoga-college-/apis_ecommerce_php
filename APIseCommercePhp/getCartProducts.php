<?php
    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }
    $userId = $_SESSION['user_id'];
    $cartId = $_SESSION['cartId'];
    #check user's shopping cart is empty or not
    if($isConnectedToDB){
        $sql = $db->prepare('SELECT COUNT(*) AS rows, id FROM carts 
        WHERE user_id = :user_id');
        $sql->bindValue(':user_id', $userId);
        $sql->execute();
        
        if($cart = $sql->fetch(PDO::FETCH_ASSOC)) {
            if((int)$cart['rows'] <= 0) {
                printError400("Your shopping cart is empty!");
                return;            
            }
        }

        #Calculate subtotal, shipping cost, and total
        #Read list of products in shopping cart     
        displayListOfProducts($userId, $cartId);
    }

?>