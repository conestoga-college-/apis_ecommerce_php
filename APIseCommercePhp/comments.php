<?php
    include 'commons.php';
    include 'dbConnection.php';
    include 'validations.php';

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        include 'retrieveComments.php';
    }
    else if ($_SERVER['REQUEST_METHOD'] == 'POST') 
    {
        include 'insertComment.php';
    }
?>