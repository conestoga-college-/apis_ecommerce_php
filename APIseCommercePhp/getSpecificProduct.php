<?php
    $productId = isset($_GET['productId']) ? (int)$_GET['productId'] : 0;

    if ($productId > 0)
    {        
        $validationMessage = validateProductId($productId);

        if($validationMessage != '') {
            printError400($validationMessage);
            return;
        }

        if($isConnectedToDB) {
            $sql = $db->prepare('SELECT id, name, image, description, price, shipping_cost FROM products 
            WHERE id = :id');
            $sql->bindValue(':id', $productId);
            $sql->execute();
            
            $response = new stdClass();

            if($product = $sql->fetch(PDO::FETCH_ASSOC)) {
                $response->id = $product['id'];
                $response->name = $product['name'];
                $response->description = $product['description'];
                $response->price = $product['price'];
                $response->shipping_cost = $product['shipping_cost'];
                $response->image = $product['image'];
                echo json_encode($response);
            }
            else {
                $response->message = "There is no product to be shown.";
                echo json_encode($response);
            }
        }
    }
    else
    { 
        printError400('Inform a valid product id in order to retrieve it.');
    }
?>