<?php
    $isConnectedToDB = false;
    try {
        #connecting to database
        $db = new PDO('mysql:dbname=ecommerce_rtr; host=localhost');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $isConnectedToDB = true;
    } catch (PDOException $e) {
        http_response_code(500);
        $response = new stdClass();
        $response->message = "Something went wrong. Do not worry, it is our fault, not yours.";
        echo json_encode($response);
    } 
?>