<?php
    function printError400($validationMessage) {
        http_response_code(400);
        $response = new stdClass();
        $response->message = $validationMessage;
        echo json_encode($response);
    }

    function printUserNotSignedIn() {
        http_response_code(401);
        $response = new stdClass();
        $response->message = 'User is not signed in';
        echo json_encode($response);
    }

    function getJsonFromPost() {
        $post = trim(file_get_contents("php://input"));
        return json_decode($post, true);
    }

    function displayListOfProducts($userId, $cartId) {
        include 'dbConnection.php';

        #Read list of products in shopping cart     
        $sql = $db->prepare('SELECT products.id, products.name, products.description, 
        products.image, products.price, products.shipping_cost, cart_items.quantity 
        FROM carts 
        INNER JOIN cart_items ON cart_items.cart_id = carts.id
        INNER JOIN products ON cart_items.product_id = products.id
        WHERE user_id = :user_id
        ORDER BY products.id');
        $sql->bindValue(':user_id', $userId);
        $sql->execute();

        $resultProducts = array();

        if($products = $sql->fetchAll(PDO::FETCH_ASSOC)) {          
            foreach($products as $product) {
                $resultProduct = new stdClass();
                $resultProduct->id = $product['id'];
                $resultProduct->name = $product['name'];
                $resultProduct->price = $product['price'];
                $resultProduct->shipping_cost = $product['shipping_cost'];
                $resultProduct->quantity = $product['quantity'];
                $resultProduct->image = $product['image'];
                array_push($resultProducts, $resultProduct);
            }
            $response = new stdClass();
            $response->cart = calculateCart($cartId);
            $response->items = $resultProducts;
            echo json_encode($response);
        }
        else {   
            $response = new stdClass();
            $response->message = "There is no product to be shown.";
            echo json_encode($response);
        } 
    }

    function isUserSignedIn() {
        if(isset($_SESSION['is_signed_in'])) {
            if($_SESSION['is_signed_in']) {
                return true;
            }
        }

        return false;
    }

    function checkIfProductExistInTheCart($userId, $productId) {
        include 'dbConnection.php';
        
        #checking if product and user are already added in shopping cart
        $sql = $db->prepare('SELECT COUNT(*) AS rows FROM carts
        INNER JOIN cart_items ON cart_items.cart_id = carts.id
        WHERE carts.user_id = :userId
        AND cart_items.product_id = :productId');
        $sql->bindValue(':userId', $userId);
        $sql->bindValue(':productId', $productId);
        $sql->execute();

        if($count = $sql->fetch(PDO::FETCH_ASSOC)){
            return (int)$count['rows'];
        } else {
            return 0;
        }
    }

    function uploadFiles($imageToUpload, $productId) {
        $targetDir = "images/uploads/";

        $fileName = $productId . '_' . basename($imageToUpload['name']);
        $targetFile = $targetDir . uniqid() . '_' . $fileName;
        $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

        $check = getimagesize($imageToUpload["tmp_name"]);

        #Check if $check is set to false or if 
        #the file could be moved
        if ($check === false || !move_uploaded_file($imageToUpload["tmp_name"], $targetFile)) {
            return '';
        }

        return './eCommercePhp/' . $targetFile;
    }

    function reArrayFiles($filePost) {
        $fileArray = array();
        $fileCount = count($filePost['name']);
        $fileKeys = array_keys($filePost);

        for ($i=0; $i < $fileCount; $i++) {
            foreach ($fileKeys as $key) {
                $fileArray[$i][$key] = $filePost[$key][$i];
            }
        }

        return $fileArray;
    }
    
    function calculateCart($cartId) {
        include 'dbConnection.php';
    
        $sql = $db->prepare('SELECT cart_items.cart_id,SUM(quantity*price) AS subtotal, SUM(shipping_cost) AS expected_shipping_cost 
        FROM products 
        INNER JOIN cart_items ON cart_items.product_id = products.id 
        WHERE cart_items.cart_id = :cartId');
        $sql->bindValue(':cartId', $cartId);
        $sql->execute();
    
        $cart = new stdClass();
        if($results = $sql->fetch(PDO::FETCH_ASSOC)){
            #calculate shopping cart
            $subtotal = round($results["subtotal"], 2);
            $tax =  round($subtotal * 0.13, 2);
            $shipping_cost = round($results["expected_shipping_cost"], 2);
            $total = round($subtotal + $tax + $shipping_cost, 2);
    
            #set value to $cart object
            $cart->cart_id = $results["cart_id"];
            $cart->subtotal = $subtotal;
            $cart->tax = $tax;
            $cart->expected_shopping_cost = $shipping_cost;
            $cart->total = $total;
            return $cart;
        } else {
            printError400("Invalid shopping cart.");
            return;
        }
    }
?>