<?php
    $userId = $_SESSION['user_id'];
    #check user's order history 
    if($isConnectedToDB){
        $sql = $db->prepare('SELECT id, order_date, order_number, status, subtotal, tax, shipping_cost, total
        FROM orders 
        WHERE user_id = :user_id');
        $sql->bindValue(':user_id', $userId);
        $sql->execute();
        
        if($orders = $sql->fetchAll(PDO::FETCH_ASSOC)) {
            echo json_encode($orders);
        }   
        else{
            printError400("You don't have a order yet!");
            return;
        }  
    }

?>