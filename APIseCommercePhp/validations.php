<?php
    function validateCreateUser($email, $password, $firstName, $lastName, 
    $userName, $shippingAddress, $billingAddress) {
        $messageResult = 'Something went wrong.';
        $isValidObject = true;

        if(validateEmail($email)) {
            $messageResult = $messageResult . ' Email is empty or in a wrong format.';
            $isValidObject = false;
        }

        if($password == '') {
            $messageResult = $messageResult . ' Password is empty.';
            $isValidObject = false;
        }

        if(validatePasswordRule($password)) {
            $messageResult = $messageResult . ' Password is does not contain a lower case letter, upper case letter and a digit.';
            $isValidObject = false;
        }

        if(validatePasswordLenghtRule($password)) {
            $messageResult = $messageResult . ' Password must be between 6 and 16 characters.';
            $isValidObject = false;
        }

        if($firstName == '') {
            $messageResult = $messageResult . ' First name is empty.';
            $isValidObject = false;
        }

        if($lastName == '') {
            $messageResult = $messageResult . ' Last Name is empty.';
            $isValidObject = false;
        }

        if($userName == '') {
            $messageResult = $messageResult . ' Username is empty.';
            $isValidObject = false;
        }

        if($shippingAddress == '') {
            $messageResult = $messageResult . ' Shipping address is empty.';
            $isValidObject = false;
        }

        if($billingAddress == '') {
            $messageResult = $messageResult . ' Billing address is empty.';
            $isValidObject = false;
        }

        return ($isValidObject == true ? '' : $messageResult);
    }

    function validateUpdateUser($email, $firstName, $lastName, $shippingAddress) {
        $messageResult = 'Something went wrong.';
        $isValidObject = true;

        if(validateEmail($email)) {
            $messageResult = $messageResult . ' Email is empty or in a wrong format.';
            $isValidObject = false;
        }

        if($firstName == '') {
            $messageResult = $messageResult . ' First name is empty.';
            $isValidObject = false;
        }

        if($lastName == '') {
            $messageResult = $messageResult . ' Last Name is empty.';
            $isValidObject = false;
        }

        if($shippingAddress == '') {
            $messageResult = $messageResult . ' Shipping address is empty.';
            $isValidObject = false;
        }

        return ($isValidObject == true ? '' : $messageResult);
    }

    function validateLogin($username, $password) {
        $messageResult = 'Something went wrong.';
        $isValidObject = true;

        if($password == '' || 
        $username == '' || 
        validatePasswordRule($password) || 
        validatePasswordLenghtRule($password)) {
            $messageResult = $messageResult . ' Wrong username or password. Please try again.';
            $isValidObject = false;
        }

        return ($isValidObject == true ? '' : $messageResult);
    }

    function validateChangePassword($password) {
        $messageResult = 'Something went wrong.';
        $isValidObject = true;

        if($password == '' ) {
            $messageResult = $messageResult . ' Please fill the password.';
            $isValidObject = false;
        }

        if(validatePasswordRule($password)) {
            $messageResult = $messageResult . ' Password is does not contain a lower case letter, upper case letter and a digit.';
            $isValidObject = false;
        }

        if(validatePasswordLenghtRule($password)) {
            $messageResult = $messageResult . ' Password must be between 6 and 16 characters.';
            $isValidObject = false;
        }

        return ($isValidObject == true ? '' : $messageResult);
    }

    function validateEmail($email) {
        return $email == '' || !filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    function validatePasswordRule($password) {
        $filter = "/((?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))/";
        return !preg_match($filter, $password);
    }

    function validatePasswordLenghtRule($password) {
        return strlen($password) < 6 || strlen($password) > 16;
    }

    function validateProductId($productId) {
        $messageResult = 'Something went wrong.';
        $isValidObject = true;

        if(!is_int($productId)) {
            $messageResult = $messageResult . ' Product ID is invalid.';
            $isValidObject = false;
        }

        return ($isValidObject == true ? '' : $messageResult);
    }

    function validateInsertComment($productId, $rating) {
        $messageResult = 'Something went wrong.';
        $isValidObject = true;

        if($productId == '') {
            $messageResult = $messageResult . ' Product ID is empty.';
            $isValidObject = false;
        }

        if(!is_numeric($productId)) {
            $messageResult = $messageResult . ' Product ID must be a number.';
            $isValidObject = false;
        }

        if($rating == '') {
            $messageResult = $messageResult . ' Rating is empty.';
            $isValidObject = false;
        }

        if(!is_numeric($rating)) {
            $messageResult = $messageResult . ' Rating must be a number.';
            $isValidObject = false;
        }

        return ($isValidObject == true ? '' : $messageResult);
    }

    function checkImageExtensionAllowed($filesArray) {
        $extensions = array("jpeg","jpg","png");
        $resultImageCheck = true;

        foreach($filesArray as $image) {
            $file_ext = explode('.', $image['name'])	;
            $file_ext = end($file_ext);

            if(in_array($file_ext, $extensions) === false) {
                $resultImageCheck = false;
            }
        }

        return $resultImageCheck;
    }

    function checkItemBought($userId, $productId){
        include 'dbConnection.php';
    
        $sql = $db->prepare('SELECT COUNT(*) AS rows
        FROM order_items 
        INNER JOIN orders ON orders.id = order_items.order_id
        INNER JOIN products ON order_items.product_id = products.id
        WHERE orders.user_id = :user_id AND products.id = :productId');
        $sql->bindValue(':user_id', $userId);
        $sql->bindValue(':productId', $productId);
        $sql->execute();
        
        if($count = $sql->fetch(PDO::FETCH_ASSOC)){
            return (int)$count['rows'];
        } else {
            return 0;
        }
    }
?>