<?php
    session_start();

    include 'commons.php';
    include 'dbConnection.php';

    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    $cartId = $_SESSION['cartId'];
    $userId = $_SESSION['user_id'];

    #validate cart id
    if(!isset($cartId) || !is_int($cartId) || $cartId <= 0) {
        printError400("Invalid cart.");
        return;
    }

    #get all the information needed to finalize the cart and
    #turn into an order
    $sql = $db->prepare('SELECT products.id, products.name, products.description, products.image, 
    products.price, products.shipping_cost, cart_items.quantity FROM carts 
    INNER JOIN cart_items ON cart_items.cart_id = carts.id
    INNER JOIN products ON cart_items.product_id = products.id
    WHERE carts.user_id = :userId
    ORDER BY products.id');
    $sql->bindValue(':userId', $userId);
    $sql->execute();

    if($products = $sql->fetchAll(PDO::FETCH_ASSOC)) {
        #create an order
        $sql = $db->prepare('INSERT INTO orders (user_id, order_date, order_number, status)
        VALUES (:userId, :orderDate, :orderNumber, :status)');
        $sql->bindValue(':userId', $userId);
        $sql->bindValue(':orderDate', date("Y-m-d"));
        $sql->bindValue(':orderNumber', uniqid());
        $sql->bindValue(':status', 1);
        $sql->execute();

        $orderId = $db->lastInsertId();

        #foreach product the user has in its cart, it is adding into the order_items table
        foreach($products as $product) {
            $sql = $db->prepare('INSERT INTO order_items (order_id, product_id, product_name, product_description, 
            product_image, product_price, product_shipping_cost, product_quantity)
            VALUES (:orderId, :productId, :productName, :productDescription, :productImage, :productPrice, 
            :productShippingCost, :productQuantity)');
            $sql->bindValue(':orderId', $orderId);
            $sql->bindValue(':productId', $product['id']);
            $sql->bindValue(':productName', $product['name']);
            $sql->bindValue(':productDescription', $product['description']);
            $sql->bindValue(':productImage', $product['image']);
            $sql->bindValue(':productPrice', $product['price']);
            $sql->bindValue(':productShippingCost', $product['shipping_cost']);
            $sql->bindValue(':productQuantity', $product['quantity']);
            $sql->execute();
        }

        #calculate subtotal, shipping cost, tax, and total 
        $sql = $db->prepare('SELECT order_items.order_id, SUM(product_quantity*product_price) AS subtotal, SUM(product_shipping_cost) AS expected_shipping_cost 
        FROM order_items 
        WHERE order_items.order_id= :order_id');
        $sql->bindValue(':order_id', $orderId);
        $sql->execute();

        if($results = $sql->fetch(PDO::FETCH_ASSOC)){
            #get order's summary
            $subtotal = round($results["subtotal"], 2);
            $expected_shipping_cost = round($results["expected_shipping_cost"], 2);
            $tax = round($subtotal * 0.13, 2);
            $total = round($subtotal + $expected_shipping_cost + $tax, 2);

            #save sumary order into db
            $cmd = 'UPDATE orders SET subtotal = :subtotal, shipping_cost = :shipping_cost, tax = :tax, total = :total  WHERE id = :id';
            $sql = $db->prepare($cmd);
            $sql->bindValue(':subtotal', $subtotal);
            $sql->bindValue(':shipping_cost', $expected_shipping_cost);
            $sql->bindValue(':tax', $tax);
            $sql->bindValue(':total', $total);
            $sql->bindValue(':id', $orderId);
            $sql->execute();
        } else {
            printError400("Invalid order items.");
            return;
        }

        #after inserting into the order_items table, deletes every item in the
        #shopping cart_items
        $sql = $db->prepare('DELETE FROM cart_items WHERE cart_id = :cartId');
        $sql->bindValue(':cartId', $cartId);
        $sql->execute();

        #get the information of the successful order finalization
        $sql = $db->prepare('
        SELECT order_number, order_date, status, subtotal, shipping_cost, tax, total
        FROM orders
        WHERE id = :orderId');
        $sql->bindValue(':orderId', $orderId);
        $sql->execute();

        if($order = $sql->fetchAll(PDO::FETCH_ASSOC)) {
            echo json_encode($order);
        }
    } else {
        printError400('There are no products in the shopping cart.');
        return;
    }
?>