<?php

    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    $json = getJsonFromPost();

    $productId = isset($json['product_id']) ? (int)$json['product_id'] : 0;
    $quantity = isset($json['quantity']) ? (int)$json['quantity'] : 0;
    $userId = $_SESSION['user_id'];

    #validate quantity
    if($productId <= 0) {
        printError400("Product is invalid.");
        return;
    }

    #validate quantity
    if($quantity <= 0) {
        printError400("Quantity is invalid.");
        return;
    }

    #reading records in products table with PHP
    $sql = $db->prepare('SELECT COUNT(*) AS rows FROM products WHERE id = :id');
    $sql->bindValue(':id', $productId);
    $sql->execute();
    
    #checking product is valid
    if($count = $sql->fetch(PDO::FETCH_ASSOC)) {
        if((int)$count['rows'] <= 0) {
            printError400("Product is invalid.");
            return;
        }
    }

    #check if the user has a cart, if not, creates one
    $sql = $db->prepare('SELECT id FROM carts WHERE user_id = :userId');
    $sql->bindValue(':userId', $userId);
    $sql->execute();

    if($cartId = $sql->fetch(PDO::FETCH_ASSOC)) {
        $_SESSION['cartId'] = (int)$cartId['id'];
    } else {
        $sql = $db->prepare('INSERT INTO carts (user_id) VALUES (:userId)');
        $sql->bindValue(':userId', $userId);
        $sql->execute();

        $_SESSION['cartId'] = (int)$db->lastInsertId();
    }
    
    $cartId = $_SESSION['cartId'];
    
    #checking if product and user are already added in shopping cart
    $sql = $db->prepare('SELECT COUNT(*) AS rows FROM carts
    INNER JOIN cart_items ON cart_items.cart_id = carts.id 
    WHERE user_id = :userId 
    AND product_id = :productId');
    $sql->bindValue(':productId', $productId);
    $sql->bindValue(':userId', $userId);
    $sql->execute();

    if($count = $sql->fetch(PDO::FETCH_ASSOC)) {
        if((int)$count['rows'] > 0) {
            printError400("Product already in the cart.");
            return;
        }
    }

    #Writing records in PHP
    $sql = $db->prepare('INSERT INTO cart_items (cart_id, product_id, quantity) VALUES (:cartId, :productId, :quantity)');
    $sql->bindValue(':cartId', $cartId);
    $sql->bindValue(':productId', $productId);
    $sql->bindValue(':quantity', $quantity);
    $sql->execute();
    
    $response = new stdClass();
    $response->message = 'Product successfully inserted into the cart.';
    echo json_encode($response);
?>