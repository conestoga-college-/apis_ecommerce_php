<?php
    session_start();
    
    include 'commons.php';
    include 'validations.php';
    include 'dbConnection.php';

    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    $userId = $_SESSION['user_id'];

    $json = getJsonFromPost();
    
    if(!isset($json['old_password'])){
        printError400("Old password cannot be empty.");
        return;
    }

    if(!isset($json['new_password'])){
        printError400("New password cannot be empty.");
        return;
    }

    $oldPassword = $json['old_password'];
    $newPassword = $json['new_password'];

    $validationMessage = validateChangePassword($newPassword);

    if($validationMessage != '') {
        printError400($validationMessage);
        return;
    }

    $newPassword = password_hash($newPassword, PASSWORD_DEFAULT);
    
    if($isConnectedToDB){
        #check user's password
        $sql = $db->prepare('SELECT password FROM users WHERE id = :user_id');
        $sql->bindValue(':user_id', $userId);
        $sql->execute();

        if($user = $sql->fetch(PDO::FETCH_ASSOC)){
            #check if old password is valid or not
            if(password_verify($oldPassword, $user["password"])){
                #update new password
                $sql = $db->prepare('UPDATE users SET password = :password
                WHERE id = :user_id');
                $sql->bindValue(':password', $newPassword);
                $sql->bindValue(':user_id', $userId);
                $sql->execute();

                $response = new stdClass();
                $response->message = "Your password has changed successfully!";
                echo json_encode($response);
            }
            else{
                printError400("Your old password is invalid!");
                return;
            }
        }
        else
        {
            printError400("The user is invalid!");
            return;
        }      
    }

?>