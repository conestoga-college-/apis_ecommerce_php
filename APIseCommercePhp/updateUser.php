<?php
    #check if user is signed in
    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    #get body from the request
    $post = trim(file_get_contents("php://input"));
    $json = json_decode($post, true);

    $email = isset($json['email']) ? $json['email'] : '';
    $firstName = isset($json['firstName']) ? $json['firstName'] : '';
    $lastName = isset($json['lastName']) ? $json['lastName'] : '';
    $shippingAddress = isset($json['shippingAddress']) ? $json['shippingAddress'] : '';

    #validates data
    $validationMessage = validateUpdateUser($email, $firstName, $lastName, $shippingAddress);

    if($validationMessage != '') {
        printError400($validationMessage);
        return;
    }

    if($isConnectedToDB) {
        $userId = $_SESSION['user_id'];

        #check if the user id exists in the DB
        $sql = $db->prepare('SELECT COUNT(0) AS ROWS FROM users WHERE id = :id');
        $sql->bindValue(':id', $userId);
        $sql->execute();

        if($count = $sql->fetch(PDO::FETCH_ASSOC)) {
            if((int)$count['ROWS'] < 1) {
                printError400('User not found.');
                return;
            }
        }

        #check if the new email address is already being used by another user
        $sql = $db->prepare('SELECT COUNT(*) AS rows 
                            FROM users 
                            WHERE email = :email
                            AND id != :id');
        $sql->bindValue(':email', $email);
        $sql->bindValue(':id', $userId);
        $sql->execute();

        if($count = $sql->fetch(PDO::FETCH_ASSOC)) {
            if((int)$count['rows'] > 0) {
                printError400('Email is already in use. Please choose another one.');
                return;
            }
        }

        #updates the DB with the information from the request body
        $sql = $db->prepare('UPDATE users SET email = :email, first_name = :firstName, last_name = :lastName, 
        shipping_address = :shippingAddress WHERE id = :id');
        $sql->bindValue(':email', $email);
        $sql->bindValue(':firstName', $firstName);
        $sql->bindValue(':lastName', $lastName);
        $sql->bindValue(':shippingAddress', $shippingAddress);
        $sql->bindValue(':id', $userId);
        $sql->execute();

        #retrieve the updated information
        $sql = $db->prepare('SELECT id, email, first_name, last_name, username,
        shipping_address FROM users WHERE id = :id');
        $sql->bindValue(':id', $userId);
        $sql->execute();

        if($user = $sql->fetch(PDO::FETCH_ASSOC)) {
            $response = new stdClass();
            $response->email = $user['email'];
            $response->first_name = $user['first_name'];
            $response->last_name = $user['last_name'];
            $response->username = $user['username'];
            $response->shipping_address = $user['shipping_address'];
            echo json_encode($response);
        }
    }
?>