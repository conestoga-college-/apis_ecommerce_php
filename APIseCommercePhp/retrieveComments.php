<?php
    if (isset($_GET['productId'])) {
        $productId = (int)$_GET['productId'];
        
        $validationMessage = validateProductId($productId);

        if($validationMessage != '') {
            printError400($validationMessage);
            return;
        }

        if($isConnectedToDB) {
            $sql = $db->prepare('SELECT product_id, user_id, rating, group_concat(image) as images, text 
                                FROM comments 
                                LEFT JOIN comment_images ON comment_images.comment_id = COMMENTS.id 
                                WHERE product_id = :product_id
                                GROUP BY product_id, user_id');
            $sql->bindValue(':product_id', $productId);
            $sql->execute();
            
            if($comments = $sql->fetchAll(PDO::FETCH_ASSOC)) {
                echo json_encode($comments);
            }
            else {
                $response = new stdClass();
                $response->message = "There is no comments to this product.";
                echo json_encode($response);
            }
        }
    }
    else { 
        printError400('Inform the Product ID in order to retrieve comments');
    }
?>