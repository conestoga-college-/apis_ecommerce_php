<?php
    session_start();

    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    $productId = $_POST['product_id'];
    $rating = $_POST['rating'];
    $text = isset($_POST['text']) ? $_POST['text'] : '';
    $images = isset($_FILES['images']) ? $_FILES['images'] : '';
    $userId = $_SESSION['user_id'];

    $validationMessage = validateInsertComment($productId, $rating);
    if($validationMessage != '') {
        printError400($validationMessage);
        return;
    }
    #check item exist in order_items table
    if(checkItemBought($userId, $productId) == 0){
        printError400("You have to buy the porduct before comment!");
        return;
    } 

    #checking some product comment is already added by that user
    $sql = $db->prepare('SELECT COUNT(*) AS rows FROM comments WHERE product_id = :product_id AND user_id = :user_id');
    $sql->bindValue(':product_id', $productId);
    $sql->bindValue(':user_id', $userId);
    $sql->execute();

    if($count = $sql->fetch(PDO::FETCH_ASSOC)) {
            if((int)$count['rows'] > 0) {
            printError400("You have already done a comment for the product.");
            return;
        }
    }

    if($images != ''){
        $filesArray = reArrayFiles($_FILES['images']);
        $resultImageCheck = checkImageExtensionAllowed($filesArray);

        if(!$resultImageCheck) {
            printError400("We only accept files with the extensions: jpeg, jpg, png");
            return;
        }
    }
    
    #Insert comments in DB
    $cmd = 'INSERT INTO comments (product_id, user_id, rating, text)'. 'VALUES (:product_id, :user_id, :rating, :text)';
    $sql = $db->prepare($cmd);
    $sql->bindValue(':product_id', $productId);
    $sql->bindValue(':user_id', $userId);
    $sql->bindValue(':rating', $rating);
    $sql->bindValue(':text', $text);
    $sql->execute();

    $newCommentId = $db->lastInsertId();

    if($images !== '') {
        foreach($filesArray as $imageToUpload) {
            $resultUpload = uploadFiles($imageToUpload, $productId);

            if($resultUpload === '') {
                printError400("The comment was successfully inserted but we could not upload
                the file.");
                return;
            }

            #Insert imagens from the comment in DB
            $cmd = 'INSERT INTO comment_images (comment_id, image) VALUES (:comment_id, :image)';
            $sql = $db->prepare($cmd);
            $sql->bindValue(':comment_id', $newCommentId);
            $sql->bindValue(':image', $resultUpload);
            $sql->execute();
        }
    }

    #returning all the comments to the product
    $sql = $db->prepare('SELECT product_id, user_id, rating, group_concat(image) as images, text 
                            FROM comments 
                            LEFT JOIN comment_images ON comment_images.comment_id = COMMENTS.id 
                            WHERE product_id = :product_id
                            GROUP BY user_id, product_id');
    $sql->bindValue(':product_id', $productId);
    $sql->execute();
    
    if($comments = $sql->fetchAll(PDO::FETCH_ASSOC)) {
        echo json_encode($comments);
    }
?>