<?php
    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    $json = getJsonFromPost();

    $productId = isset($json['product_id']) ? (int)$json['product_id'] : 0;
    $userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';

    #validate product id
    if(!is_int($productId) || $productId <= 0){
        printError400("Product ID is invalid.");
        return;
    }
    
    $count = checkIfProductExistInTheCart($userId, $productId);
    
    #checking product is exist in shopping cart
    if($count <= 0) {
        $validationMessage = "Product is not in the cart.";
        printError400($validationMessage);
        return;
    }

    $cartId = $_SESSION['cartId'];
    
    #Deleting records in the carts table in the DB
    $sql = $db->prepare('DELETE FROM cart_items WHERE product_id = :productId AND cart_id = :cartId');
    $sql->bindValue(':productId', $productId);
    $sql->bindValue(':cartId', $cartId);
    $sql->execute();
    
    #Calculate subtotal, shipping cost, and total
    #Read list of products in shopping cart     
    displayListOfProducts($userId, $cartId);
?>