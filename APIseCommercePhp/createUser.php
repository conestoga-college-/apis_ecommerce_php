<?php
    $post = trim(file_get_contents("php://input"));
    $json = json_decode($post, true);
    $email = $json['email'];
    $password = $json['password'];
    $firstName = $json['firstName'];
    $lastName = $json['lastName'];
    $userName = $json['userName'];
    $shippingAddress = $json['shippingAddress'];
    $billingAddress = $json['billingAddress'];

    $validationMessage = validateCreateUser($email, $password, $firstName, $lastName, 
    $userName, $shippingAddress, $billingAddress);

    if($validationMessage != '') {
        printError400($validationMessage);
        return;
    }

    $password = password_hash($password, PASSWORD_DEFAULT);

    if($isConnectedToDB) {
        $sql = $db->prepare('SELECT COUNT(0) AS ROWS FROM users WHERE email = :email OR username = :username');
        $sql->bindValue(':email', $email);
        $sql->bindValue(':username', $userName);
        $sql->execute();

        if($count = $sql->fetch(PDO::FETCH_ASSOC)) {
            if((int)$count['ROWS'] > 0) {
                printError400('Please choose another email or username for registration.');
                return;
            }
        }

        $sql = $db->prepare('INSERT INTO users (email, password, first_name, last_name, 
        username, shipping_address, billing_address) 
        values (:email, :password, :firstName, :lastName, :userName, :shippingAddress, :billingAddress)');
        $sql->bindValue(':email', $email);
        $sql->bindValue(':password', $password);
        $sql->bindValue(':firstName', $firstName);
        $sql->bindValue(':lastName', $lastName);
        $sql->bindValue(':userName', $userName);
        $sql->bindValue(':shippingAddress', $shippingAddress);
        $sql->bindValue(':billingAddress', $billingAddress);
        $sql->execute();

        $newId = $db->lastInsertId();

        if($newId > 0) {
            http_response_code(200);
            $response = new stdClass();
            $response->message = "Successful creation of the user.";
            echo json_encode($response);
        }
    }
?>