<?php
    session_start();
    
    include 'commons.php';
    include 'dbConnection.php';
    include 'validations.php';

    $json = getJsonFromPost();

    $username = $json['username'];
    $password = $json['password'];

    $validationMessage = validateLogin($username, $password);

    if($validationMessage != '') {
        printError400($validationMessage);
        return;
    }

    if($isConnectedToDB) {
        $sql = $db->prepare('SELECT id, email, password, first_name, last_name, username,
        shipping_address FROM users WHERE username = :username');
        $sql->bindValue(':username', $username);
        $sql->execute();

        if($user = $sql->fetch(PDO::FETCH_ASSOC)) {
            if(!password_verify($password, $user['password'])) {
                printError400('Wrong username or password. Please try again.');
                return;
            } else {
                $userId = $user['id'];

                $sql = $db->prepare('SELECT id FROM carts WHERE user_id = :userId');
                $sql->bindValue(':userId', $userId);
                $sql->execute();

                if($cartId = $sql->fetch(PDO::FETCH_ASSOC)) {
                    $_SESSION['cartId'] = (int)$cartId['id'];
                }
                
                $_SESSION['user_id'] = $userId;
                $_SESSION['is_signed_in'] = true;

                $response = new stdClass();
                $response->email = $user['email'];
                $response->first_name = $user['first_name'];
                $response->last_name = $user['last_name'];
                $response->username = $user['username'];
                $response->shipping_address = $user['shipping_address'];
                echo json_encode($response);
            }
        } else {
            printError400('Wrong username or password. Please try again.');
            return;
        }
    }
?>