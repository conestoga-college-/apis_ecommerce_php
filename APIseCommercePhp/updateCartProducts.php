<?php
    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    $json = getJsonFromPost();

    $productId = isset($json['product_id']) ? (int)$json['product_id'] : 0;
    $quantity = isset($json['quantity']) ? (int)$json['quantity'] : 0;
    $userId = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : '';

    #validate product id
    if(!is_int($productId) || $productId <= 0){
        printError400("Product ID is invalid.");
        return;
    }

    #validate quantity
    if(!is_int($quantity) || $quantity <= 0){
        printError400("Quantity is invalid.");
        return;
    }
        
    #checking if product and user are already added in shopping cart
    $count = checkIfProductExistInTheCart($userId, $productId);
    
    #checking product is exist in shopping cart
    if($count <= 0) {
        printError400("Product is not in the cart yet.");
        return;
    }

    $cartId = $_SESSION['cartId'];

    #update quantity of product
    $cmd = 'UPDATE cart_items SET quantity = :quantity WHERE cart_id = :cartId AND product_id = :productId';
    $sql = $db->prepare($cmd);
    $sql->bindValue(':quantity', $quantity);
    $sql->bindValue(':cartId', $cartId);
    $sql->bindValue(':productId', $productId);
    $sql->execute();
    
    #Calculate subtotal, shipping cost, and total
    #Read list of products in shopping cart     
    displayListOfProducts($userId, $cartId);
    
?>