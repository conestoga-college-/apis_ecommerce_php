-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2019 at 01:50 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce_rtr`
--
CREATE DATABASE IF NOT EXISTS `ecommerce_rtr` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ecommerce_rtr`;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
CREATE TABLE `carts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `user_id`) VALUES
(5, 45),
(7, 50);

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE `cart_items` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `cart_id`, `product_id`, `quantity`) VALUES
(5, 5, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(5) NOT NULL,
  `text` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `product_id`, `user_id`, `rating`, `text`) VALUES
(1, 4, 45, 4, 'I love peeled garlic.'),
(6, 3, 45, 3, 'My kids love it!'),
(9, 4, 47, 3, 'It is always fresh!'),
(10, 2, 46, 5, 'Fresh broccoli'),
(11, 2, 47, 5, 'They have greate products and broccoli is always green and fresh.'),
(12, 2, 45, 5, 'I used to build trees on my kids plate and they love eat it.'),
(13, 7, 47, 5, 'Did you try strwberries with condensed milk? OMG!'),
(14, 8, 47, 5, 'Use them to do a sauce. But only when they are very red'),
(15, 9, 47, 5, 'Apples are also good to make a juice'),
(18, 1, 50, 4, ' We must learn how to keep bananas fresh for long time. Look at mines!'),
(21, 3, 50, 5, ' It is always a good option for a healthy meal '),
(28, 5, 50, 5, ' Look! So yummy');

-- --------------------------------------------------------

--
-- Table structure for table `comment_images`
--

DROP TABLE IF EXISTS `comment_images`;
CREATE TABLE `comment_images` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment_images`
--

INSERT INTO `comment_images` (`id`, `comment_id`, `image`) VALUES
(6, 15, './eCommercePhp/images/uploads/5cecc0652825e_9_garlic_bulb111111111.jpg'),
(7, 15, './eCommercePhp/images/uploads/5cecc06528dfa_9_tomatoes2222.jpg'),
(8, 18, './eCommercePhp/images/uploads/5cedb24ac0378_1_bananas.jpg'),
(9, 28, './eCommercePhp/images/uploads/5cedc6e979941_5_Lychee.jpg'),
(10, 28, './eCommercePhp/images/uploads/5cedc6e97b494_5_Lychee2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `order_number` varchar(255) NOT NULL,
  `subtotal` decimal(6,2) NOT NULL,
  `tax` decimal(6,2) NOT NULL,
  `shipping_cost` decimal(6,2) NOT NULL,
  `total` decimal(6,2) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `order_date`, `order_number`, `subtotal`, `tax`, `shipping_cost`, `total`, `status`) VALUES
(10, 50, '2019-05-29', '5cedb13fdae20', '13.75', '1.79', '2.25', '17.79', 1),
(11, 50, '2019-05-29', '5cedbc8d8d674', '10.68', '1.39', '1.59', '13.66', 1),
(12, 50, '2019-05-29', '5cedbd61612ab', '4.20', '0.55', '0.60', '5.35', 1),
(13, 50, '2019-05-29', '5cedbe12c25fb', '2.80', '0.36', '0.50', '3.66', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(200) NOT NULL,
  `product_description` varchar(300) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_price` decimal(6,2) NOT NULL,
  `product_shipping_cost` decimal(6,2) NOT NULL,
  `product_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `product_id`, `product_name`, `product_description`, `product_image`, `product_price`, `product_shipping_cost`, `product_quantity`) VALUES
(18, 10, 1, 'Bananas', 'Soft, sweet and delicious, the banana is a popular choice for breakfast, snacks or any time of the day. The vibrant yellow peel provides natural protection while storing, and is a great indicator of when the fruit is ready to eat!', './eCommercePhp/images/products/bananas.jpg', '0.15', '0.05', 1),
(19, 10, 2, 'Broccoli', 'Broccoli is a form of a cultivated cruciferous plant whose leafy stalks and clusters of buds are eaten as a vegetable. It is terrific steamed as a side dish or raw in a salad.', './eCommercePhp/images/products/broccoli.jpg', '1.40', '0.50', 1),
(20, 10, 3, 'Brussels Sprouts', 'Enjoy Brussel sprouts roasted, steamed, boiled, or grilled. Excellent source of vitamin K and high in fibre. Try this vegetable for your next side dish. ', './eCommercePhp/images/products/brussels_sprouts.jpg', '3.30', '1.00', 1),
(21, 10, 5, 'Lychee', 'The finest handpicked Lychees.', './eCommercePhp/images/products/lychee.jpg', '1.50', '0.60', 5),
(22, 10, 9, 'Apple', 'The mild yet sweet flavour of the Gala apple has made it a family favourite for snacks, salads and sauces. The vibrant red mixes in with subtle hints of green to give the apple its unmistakable look.', './eCommercePhp/images/products/apple.jpg', '0.70', '0.10', 2),
(23, 11, 5, 'Lychee', 'The finest handpicked Lychees.', './eCommercePhp/images/products/lychee.jpg', '1.50', '0.60', 2),
(24, 11, 6, 'Orange', 'Sweet, juicy and seedless.', './eCommercePhp/images/products/orange.jpg', '0.87', '0.10', 2),
(25, 11, 7, 'Strawberries', 'Sweet & succulent. Blitz with low fat yogurt and oats for a super smoothie.', './eCommercePhp/images/products/strawberries.jpg', '2.50', '0.80', 2),
(26, 11, 8, 'Tomatoes', 'This great Ideal tomato is perfect to eat in a sandwich because of the size of the product !The name says it all, it\'s a large tomato with a great taste, ideal for tomato lovers!', './eCommercePhp/images/products/tomatoes.jpg', '0.47', '0.09', 2),
(27, 12, 2, 'Broccoli', 'Broccoli is a form of a cultivated cruciferous plant whose leafy stalks and clusters of buds are eaten as a vegetable. It is terrific steamed as a side dish or raw in a salad.', './eCommercePhp/images/products/broccoli.jpg', '1.40', '0.50', 2),
(28, 12, 9, 'Apple', 'The mild yet sweet flavour of the Gala apple has made it a family favourite for snacks, salads and sauces. The vibrant red mixes in with subtle hints of green to give the apple its unmistakable look.', './eCommercePhp/images/products/apple.jpg', '0.70', '0.10', 2),
(29, 13, 2, 'Broccoli', 'Broccoli is a form of a cultivated cruciferous plant whose leafy stalks and clusters of buds are eaten as a vegetable. It is terrific steamed as a side dish or raw in a salad.', './eCommercePhp/images/products/broccoli.jpg', '1.40', '0.50', 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(300) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` decimal(6,2) NOT NULL,
  `shipping_cost` decimal(6,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `image`, `price`, `shipping_cost`) VALUES
(1, 'Bananas', 'Soft, sweet and delicious, the banana is a popular choice for breakfast, snacks or any time of the day. The vibrant yellow peel provides natural protection while storing, and is a great indicator of when the fruit is ready to eat!', './eCommercePhp/images/products/bananas.jpg', '0.15', '0.05'),
(2, 'Broccoli', 'Broccoli is a form of a cultivated cruciferous plant whose leafy stalks and clusters of buds are eaten as a vegetable. It is terrific steamed as a side dish or raw in a salad.', './eCommercePhp/images/products/broccoli.jpg', '1.40', '0.50'),
(3, 'Brussels Sprouts', 'Enjoy Brussel sprouts roasted, steamed, boiled, or grilled. Excellent source of vitamin K and high in fibre. Try this vegetable for your next side dish. ', './eCommercePhp/images/products/brussels_sprouts.jpg', '3.30', '1.00'),
(4, 'Garlic Bulb', 'Garlic is grown for its nutritious bulbs that contain 4 to 6 cloves and is used in many cooking recipes.', './eCommercePhp/images/products/garlic_bulb.jpg', '2.30', '1.00'),
(5, 'Lychee', 'The finest handpicked Lychees.', './eCommercePhp/images/products/lychee.jpg', '1.50', '0.60'),
(6, 'Orange', 'Sweet, juicy and seedless.', './eCommercePhp/images/products/orange.jpg', '0.87', '0.10'),
(7, 'Strawberries', 'Sweet & succulent. Blitz with low fat yogurt and oats for a super smoothie.', './eCommercePhp/images/products/strawberries.jpg', '2.50', '0.80'),
(8, 'Tomatoes', 'This great Ideal tomato is perfect to eat in a sandwich because of the size of the product !The name says it all, it\'s a large tomato with a great taste, ideal for tomato lovers!', './eCommercePhp/images/products/tomatoes.jpg', '0.47', '0.09'),
(9, 'Apple', 'The mild yet sweet flavour of the Gala apple has made it a family favourite for snacks, salads and sauces. The vibrant red mixes in with subtle hints of green to give the apple its unmistakable look.', './eCommercePhp/images/products/apple.jpg', '0.70', '0.10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(70) NOT NULL,
  `username` varchar(255) NOT NULL,
  `shipping_address` varchar(255) NOT NULL,
  `billing_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `first_name`, `last_name`, `username`, `shipping_address`, `billing_address`) VALUES
(45, 'thao@b.com', '$2y$10$Xk1gphTDTWNhp1xVbKF/LeQlFX/WW6vHTxscEVOseNUD/XLULnj8K', 'Thao', 'Thi', 'thaothi', 'Shipping Address 1', 'Billing Address 1'),
(46, 'rafael@b.com', '$2y$10$CoJhaF6ldcY1leK4KVkGAekoRUE3vxj0WxPWs3lfCO1uhGJ8qScwC', 'Rafael', 'Meneghelo', 'rmeneghelo', 'Shipping Address 1', 'Billing Address 1'),
(47, 'roberta@b.com', '$2y$10$bGMt4avHNCrc9UXcDPWHB.rTChTsF29xk6pwJaJvbDMMZCBOPTJDa', 'Roberta', 'Martins', 'robeape', 'Shipping Address 1', 'Billing Address 1'),
(50, 'aa@bb.com', '$2y$10$.ipR62etNCM4NI1CFkYYFeTVvExJjPjSyFG8HHPTv2GMCw3amj4N.', 'Mariana', 'Martins', 'marimar', 'Shipping Address 1', 'Billing Address 1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_fk_carts` (`user_id`);

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_items_cart_id_fk` (`cart_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id_fk` (`product_id`),
  ADD KEY `user_id_fk` (`user_id`);

--
-- Indexes for table `comment_images`
--
ALTER TABLE `comment_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_image_comment_pk` (`comment_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_fk` (`user_id`) USING BTREE;

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_items_order_id_fk` (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `comment_images`
--
ALTER TABLE `comment_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `carts`
--
ALTER TABLE `carts`
  ADD CONSTRAINT `user_id_fk_carts` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD CONSTRAINT `cart_items_cart_id_fk` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `product_id_fk` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `comment_images`
--
ALTER TABLE `comment_images`
  ADD CONSTRAINT `comment_image_comment_pk` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
