<?php
    session_start();

    include 'commons.php';
    include 'dbConnection.php';

    if(!isUserSignedIn()) {
        printUserNotSignedIn();
        return;
    }

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        if(isset($_GET['order_id']))
        {
            include 'getSpecificOrder.php';
        }
        else {
            include 'getOrders.php';
        }
        
    }
?>