# APIs_eCommerce_php

Apis developed in Php for an imaginary eCommerce

Technologies
Server Apache/PHP
Client Postman
Database MySQL

Task
Working in groups of up to 3, create a PHP implementation of the server components required to provide data from a MySQL database instance to the VueJS client described in Project 4.
To complete this project, you will need database entities like the following:
• Product (description, image, pricing, shipping cost)
• User (email, password, username, purchase history, shipping address)
• Comments (product, user, rating, image(s), text)
• Cart (products, quantities, user)

The use of PHP frameworks such as Laravel or others is not permitted.