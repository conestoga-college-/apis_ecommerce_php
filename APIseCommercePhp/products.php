<?php
    include 'commons.php';
    include 'dbConnection.php';
    include 'validations.php';

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        if(isset($_GET['productId']))
        {
            include 'getSpecificProduct.php';
        }
        else {
            include 'getProducts.php';
        }
        
    }
?>