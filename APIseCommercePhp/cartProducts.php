<?php

    session_start();
    
    include 'commons.php';
    include 'dbConnection.php';

    if($_SERVER['REQUEST_METHOD'] == 'GET') {
        include 'getCartProducts.php';
    } else if($_SERVER['REQUEST_METHOD'] == 'POST') {
        include 'insertCartProducts.php';
    } else if($_SERVER['REQUEST_METHOD'] == 'PUT') {
        include 'updateCartProducts.php';
    } else if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
        include 'deleteCartProducts.php';
    }
?>