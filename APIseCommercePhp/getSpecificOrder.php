<?php
    $order_id = isset($_GET['order_id']) ? (int)$_GET['order_id'] : 0;
    $userId = $_SESSION['user_id'];
    
    if(!is_int($order_id) || $order_id <= 0){
        printError400("Order ID is invalid.");
        return;
    }
    
    #check order number is valid or not
    if($isConnectedToDB) {
        $sql = $db->prepare('SELECT COUNT(*) AS rows
        FROM orders 
        WHERE user_id = :user_id
        AND id = :order_id');
        $sql->bindValue(':user_id', $userId);
        $sql->bindValue(':order_id', $order_id);
        $sql->execute();
        
        if($orders = $sql->fetch(PDO::FETCH_ASSOC)) {
            if((int)$orders['rows'] <= 0) {
                printError400("Your order number is invalid!");
                return;
            }
        }   
        
        #get summary information of the order
        $sql = $db->prepare('SELECT id, order_date, order_number, status, subtotal, tax, shipping_cost, total
        FROM orders
        WHERE user_id = :user_id
        AND id = :order_id');
        $sql->bindValue(':user_id', $userId);
        $sql->bindValue(':order_id', $order_id);
        $sql->execute();

        $result = new stdClass();
        if($order = $sql->fetch(PDO::FETCH_ASSOC)){
            $result->order = $order;

            #get detail items in the order
            $sql = $db->prepare('SELECT id, order_id, product_name, product_image, 
            product_price, product_shipping_cost, product_quantity
            FROM order_items 
            WHERE order_id = :order_id');
            $sql->bindValue(':order_id', $order_id);
            $sql->execute();

            if($items = $sql->fetchAll(PDO::FETCH_ASSOC)){
                foreach($items as $item){
                    $order_items = new stdClass();
                    $order_items->order_items_id = $item['id'];
                    $order_items->name = $item['product_name'];
                    $order_items->price = $item['product_price'];
                    $order_items->shipping_cost = $item['product_shipping_cost'];
                    $order_items->quantity = $item['product_quantity'];
                    $order_items->image = $item['product_image'];
                    $result->items[] = $order_items;      
                }
            }
            echo json_encode($result);
        }
        else{
            printError400("Your order number is invalid!");
            return;
        }
    }
?>