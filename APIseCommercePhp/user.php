<?php
    session_start();

    include 'commons.php';
    include 'dbConnection.php';
    include 'validations.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        include 'createUser.php';
    } else if($_SERVER['REQUEST_METHOD'] == 'PUT') {
        include 'updateUser.php';
    }
?>